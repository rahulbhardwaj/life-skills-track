# Listening and Active Communication

## Question 1 - What are the steps/strategies to do Active Listening?

1. Try to focus on the speaker and don't get distracted by your thought.
2. Let the speaker finish and then respond and try not to interrupt.
3. Use door openers to show you are interested.
4. Show that you are listening with body language.
5. If appropriate take notes during important conversations.
6. Paraphrase what others have said to make sure you are on the same page.

## Question 2 - According to Fisher's model, what are the key points of Reflective Listening?

- Actively listening to what the other person is saying with full attention and focus.
- Paraphrasing what the other person has said so that you have understood them correctly. This also helps to clarify any misunderstandings.
- Empathy towards the speaker by understanding their feelings and perspective.
- Asking open-ended questions to encourage the speaker to elaborate on their thoughts and feelings to have meaningful conversations.
- Non-verbal communication such as body language and tone of voice, to gain a better understanding of their emotions and thoughts.
  
## Question 3 - What are the obstacles in your listening process?

- Distractions: Sometimes I get distracted by external factors, such as noise or a busy environment, which makes it difficult to focus on the speaker's message.
- Lack of interest: Sometimes I lack interest in the topic, and it is difficult to stay focused and engaged.
- Emotional state: Sometimes being in an emotional state, such as feeling anxious, angry, or upset, makes it difficult to listen objectively and stay focused.

## Question 4 - What can you do to improve your listening?

- Use Fisher's technique of reflective listening that involves active listening, paraphrasing, empathy, open-ended questions, and paying attention to non-verbal communication.
- Try not to get distracted by external factors.
- Try to show interest with your body language.
- Try to clear your mind before active listening.
- Practice active listening.

## Question 5 - When do you switch to a Passive communication style in your day-to-day life?

- When the topic is sensitive or controversial, I might avoid expressing my opinion to avoid offending others or starting an argument.
- When I lack confidence in my communication skills, I might communicate passively by avoiding eye contact or speaking in a soft tone.
- When I want to avoid responsibility for my actions or decisions, I might communicate passively by avoiding direct communication or blaming others.

## Question 6 - When do you switch to Aggressive communication styles in your day-to-day life?

- When I feel my boundaries are being crossed, I might communicate aggressively to assert myself and protect my rights.
- When I feel threatened or attacked, I might respond aggressively to defend myself and assert my position.
- When I want to intimidate or dominate others, In some cases, I might communicate aggressively to intimidate or dominate others, especially if I feel that they are weaker or inferior to me.

## Question 7 - When do you switch into Passive Aggressive (sarcasm/gossiping/taunts/silent treatment and others) communication styles in your day-to-day life?

- When I feel frustrated or angry with someone, I might use sarcasm.
- When I feel hurt or upset by someone, I might gossip about them to others.
- When I feel resentful or angry towards someone, I might use taunts or insults.
- When I feel upset or hurt by someone, I might use silent treatment as a way of punishing them.

## Question 8 - How can you make your communication assertive? You can analyze the videos and then think about what steps you can apply in your own life?

- Learn to recognize and name your feelings: Be aware of emotions and learn to understand the needs and communicate clearly to others.
- Practice looking for the need behind the need: Instead of focusing on the surface-level issues, try to identify the underlying needs that are driving the communication to find common ground with the other person and clear conflicts more effectively.
- Start with low-stake situations: To practice assertive communication in situations such as with friends or family members to build confidence and skills to tackle more challenging situations.
- Be aware of your body language and tone: To make sure body language and tone of voice are consistent with the message.
- Don't wait to speak up about problematic situations: To address problems as soon as they arise, rather than letting them fester and become more difficult to resolve.