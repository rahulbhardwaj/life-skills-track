# Learning Process

## 1. How to Learn Faster with the Feynman Technique
- Video - 5:47 minutes - https://www.youtube.com/watch?v=_f-qkGJBPts

### Question 1 - What is the Feynman Technique? Paraphrase the video in your own words.
- Feynman Technique is a technique for how to learn and clearing any doubt on a topic.
- By following certain steps on how to learn any topic.
- So that you can explain to anyone having no base knowledge to grasp that concept.
- The process consists of 4 steps as
  - Take a paper and start with the topic name on it first.
  - Explain it as simply as possible, using examples.
  - Identify what is wrong or missing by referring to the source.
  - Find any complicated terms or examples and try to simplify them.
- The core aspect of this technique can be - can you explain the topic to a kid?
  
### Question 2 - What are the different ways to implement this technique in your learning process?
- While learning any new technique, you can follow the steps to get it clear.
- If you starting a new concept, first learn it then use the technique to get it fully understand.
- If you are a developer and don't want to write it down, you can make a simple video explaining it and watch it again if it provides a simple explanation or not.

## 2. Learning How to Learn TED talk by Barbara Oakley
- Video - 17:50 minutes - https://www.youtube.com/watch?v=O96fE1E-rf8
  
### Question 3 - Paraphrase the video in detail in your own words.
- You can learn anything despite how you think your brain works.
- Anyone can change their brain on how to grasp concepts and how to learn easily and effectively.
- One theory is focus and diffuse, which states as the brain works in two modes - focus and relaxed you can learn in focus mode but if you are trying hard to understand, you can relax while thinking of that concept that you fall asleep. While doing so you can keep a ball in hand which will fall and wake you up and you will remember all the thought process. You can also use focus and diffuse to be creative and think of a solution to a new problem.
- Get away from procrastination by scheduling your next hour as to do only work for 25 minutes (use a timer) with no distractions like smartphones with focus and then relax for a couple of minutes rather than thinking I will do it later. This will also enhance your work performance.
- You have to work hard if you are a slow thinker, and your learning speed will improve in the future as you are practicing so your brain also learns how to learn.
- Effective study and practice - you can study all day but while using those concepts you find out you can't use them like if you are learning how to ride a bike by just watching a video, it will not work.
- You have to practice the concepts, again and again, to be able to use them fully and properly.

### Question 4 - What are some of the steps that you can take to improve your learning process?
- While solving a new problem, you can use focus and diffuse but it is a time-intensive process.
- While learning a new concept or learning any complex theory for 25 minutes and relaxing for a couple of minutes.
- Effective learning by referring to more than one source of information.
- Practicing the concept by solving problems based on them and problems should be of all kinds in the given conceptual scope.
- As a developer, you should practice new concepts by solving problems based on them on different platforms.

## 3. Learn Anything in 20 hours
- Video - 20 minutes - https://www.youtube.com/watch?v=5MgBikgcWnY
  
### Question 5 - Your key takeaways from the video? Paraphrase your understanding.
- Learning skills seem like a lot difficult to perfect and also you do not have enough time to learn that fully as life is very busy and thinking of I do not have time to do that.
- So, a question arises, how much time is required to learn a new skill you will find many articles stating ten thousand hours which is equivalent to 5 years of daily work.
- These ten thousand hours became profound on the wrong conclusion of research on how much time it takes to become perfect at a very high level of skill or best in very competitive skill like C Ronaldo in football, to conclude it will take ten thousand hours to learn anything.
- Then, how much time is actually required, answer is 20 hours of focus and effective learning and practice which can be done in a month with 45 minutes of daily learning and practice.
- According to some researchers, the learning curve increases dramatically with the increase in practice, so the more you practice more you learn.
- There are four steps to rapid skill acquisition:
  - Deconstruct the skill - The first barrier is to figure out why you learning that skill and what parts of that skill you will actually use so as to focus learning on them and practicing.
  - Learn enough to self-correct - learn enough to improve on mistakes you are making while using it.
  - Remove Practice Barriers - get the environment or get the things you require for practicing.
  - Practice for 20 hours - you will overcome the thought that you can't learn this skill
  
### Question 6 - What are some of the steps that you can while approaching a new topic?
- To not think that you cant learn new skills.
- To learn a new skill in a month by giving 1 hour of learning and practice on it.
You don't have to learn the whole skillset, you have to only learn parts of it that will be used for you.