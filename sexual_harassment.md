# Prevention of Sexual Harassment

## Overview
- Sexual harassment is a type of harassment that includes unwelcome sexual advances, requests for sexual favors, and other verbal or physical harassment of a sexual nature in the workplace or public places.
- Sexual harassment under the Indian govt. is covered by
    - The Sexual Harassment at The Workplace Act,2013
    - The Indecent Representation of Women Act,1987
    - Laws under IPC Section 345(A) , Section 209 and Section 509


## What kinds of behavior cause sexual harassment
Here are some basic types of sexual harassment behaviors:
- Quid Pro Quo
  - When an employer, manager, or boss requests sexual favors in exchange for any job activity or promotion.
  - Also, when they threaten to fire or demote them if not accepting those sexual favors.
- Hostile Work Environment
  - When an employer or coworker tries to make unwelcome sexual conduct towards any employee.
  - It can interfere with work performance and ethics.
- Physical Harassment
  - Any unwelcome sexual advances or any attempts to force sexual conduct including rape.
  - Any physical contact that makes a person uncomfortable can also be unintentional.
- Verbal Harassment*
  - Commenting about physical appearance.
  - Constant flirting with coworkers without any consent.
  - Unwelcome conversations between coworkers and jokes that are sexual in nature.
- Non-Verbal Harassment
  - Staring at a person or following someone intentionally.
  - Gestures that are sexual in nature.
  - Forced to play along, when a person forces to go along because of how he is.


## What would you do in case you face or witness any incident or repeated incidents of such behavior?
- If you face or witness sexual harassment, you can follow some guidelines that are appropriate to access and control the situation and not to make it more corrosive unintentionally.
Here is one of the common steps you can follow, it stands for CARE
  - Create a Distraction: Intervene directly by speaking or any other means to create a distraction to stop the harasser or to stop the situation to escalate, and do not put yourself in danger.
  - Ask Directly: Talk to the person who is being harassed to make him/her feel better. Ask them if they are alright or if they are upset, and offer him/her to spend some time.
  - Refer to an Authority: Bring someone or contact someone who has some authority like the security guard to be the safest way to intervene in between.
  - Enlist others: It can be hard to control the situation alone and for your own safety enlist help from friends and others around.

## References
- https://en.wikipedia.org/wiki/Sexual_harassment
- https://www.rainn.org/articles/sexual-harassment
- https://www.livemint.com/Politics/XgRdygHg297gwYMYPjNSuI/What-is-sexual-harassment-under-Indian-laws.html
- https://smitheylaw.com/what-kinds-of-behaviors-are-considered-sexual-harassment/