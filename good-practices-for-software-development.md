# Good Practices for Software Development

## Question 1 - What is your one major takeaway from each one of the 6 sections? So 6 points in total.

- Make sure to ask questions and seek clarity in the meeting itself. Since most of the time, it will be difficult to get the same set of people online again
- Requirements have changed? - If the current deadline will not be met due to the changes, inform relevant team members about the new deadline
- Explain the problem clearly, and mention the solutions you tried out to fix the problem.
- Make time for your company, the product you are working on, and your team members. This will help a lot in improving your communication with the team.
- Remember they have their own work to do as well. Pick and choose your communication medium depending on the situation
- Make sure you manage your food situation well. Too little food or too much food lead to lower levels of concentration.

## Question 2 - Which area do you think you need to improve on? What are your ideas to make progress in that area?

- Actively participate in meetings and ask questions when you need clarity.
- Explain any problems clearly and mention the solutions you have tried to fix the problem.
- Make sure everyone has a clear understanding of the project goals and requirements.
- Prioritize your work and plan your day.
- Allocate time for your company, the product you are working on, and your team members.
- Pick and choose your communication medium depending on the situation to communicate more effectively.
- Manage your food situation well by eating healthy and balanced meals and avoiding skipping meals or overeating.


# References
- [Mountblue Github | Good Practices for Software Development](https://github.com/mountblue/life-skills-track/blob/main/good-practices-for-software-development.md)