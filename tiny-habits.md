# Tiny Habits

## Tiny Habits - BJ Fogg

### Question 1 - Your takeaways from the video (Minimum 5 points)

- Tiny Habits are simple, easy-to-do actions that can be incorporated into our daily routines. They can be anything from doing two push-ups after brushing your teeth to flossing one tooth every day.
- Celebrating small successes is important because it reinforces the behavior and builds momentum. Even something as simple as saying "I did it!" or giving yourself a mental high-five can help.
- Anchoring the new behavior to an existing habit, such as doing ten squats after sitting down at your desk, can help to make it stick. This is because the existing habit provides a trigger for the new behavior.
- Habits are formed through repetition, so it's better to start small and gradually increase the difficulty of the behavior.
- Making big, drastic changes all at once can be overwhelming and often leads to failure. Fogg emphasizes the importance of starting with small, manageable changes and building from there.
- Using positive self-talk and framing the behavior in a positive light can help to motivate you to stick with it. For example, instead of saying "I have to floss my teeth," say "I get to floss my teeth and have a healthy smile."

## Tiny Habits by BJ Fogg - Core Message

### Question 2 - Your takeaways from the video in as much detail as possible

- The first part, Shrink, is about finding the tiniest version of a desired habit. The idea is that starting with a small, manageable habit will require less motivation and will be easier to maintain. Fogg suggests finding a behavior that can be done in 30 seconds or less, such as doing one push-up or writing down one to-do item on an index card.

- The second part, Action Prompts, is about creating a prompt that will remind you to perform your tiny habit. There are three types of prompts: external prompts (such as alarms or notifications), internal prompts (such as thoughts or emotions), and action prompts (which use the completion of one behavior to trigger the next). Fogg suggests using action prompts, as they are less disruptive and more motivating. For example, if you use the completion of cleaning your kitchen countertops as the prompt to take out the garbage, you can leverage the momentum you already have from being on your feet and walking around the kitchen.

- The third part, Shine, is about growing and nurturing the habit. Once you have established your tiny habit and action prompt, you can continue to build on it by adding a little bit of shine. This means celebrating and rewarding yourself for completing the habit, as well as finding ways to make it more enjoyable or meaningful. For example, if your tiny habit is to do one push-up every morning, you could add some shine by listening to your favorite music while you do it.

- Overall, the Tiny Habits method is designed to help people develop new habits by starting small and building momentum through action prompts and positive reinforcement. By focusing on tiny habits that require little motivation and can be done quickly, it becomes easier to create a consistent habit that can lead to bigger changes in your life.

### Question 3 - How can you use B = MAP to make making new habits easier?

- B = MAP stands for Behavior equals Motivation, Ability, and Prompt.
- According to B = MAP, to make a new habit easier, you need to increase your motivation, ability, and prompts.
- Motivation can be increased by making the new habit more enjoyable, connecting it to your values, or visualizing the benefits.
- Ability can be increased by breaking the habit down into smaller steps, making it easier to do, or removing obstacles that make it harder.
- The prompt can be increased by setting reminders, making the new habit a part of your routine, or using a habit tracker.
- By using B = MAP to address all three elements of behavior, you can create a more sustainable and lasting habit.

### Question 4 - Why it is important to "Shine" or Celebrate after each successful completion of a habit?

- Celebrating successes creates positive emotions: Celebrating success after completing a habit reinforces positive emotions, such as happiness and satisfaction. These positive emotions motivate you to continue with the habit and make it a part of your routine.

- Celebrating success builds momentum: Celebrating your success after each completed habit creates momentum and makes it easier to maintain the habit. The more you celebrate success, the more momentum you build, and the more likely you are to continue with the habit.

- Celebrating success increases self-confidence: Celebrating your success after completing a habit increases your self-confidence. When you feel confident in your ability to complete a habit successfully, you are more likely to continue with the habit and take on other challenges in life.

- Celebrating success provides a sense of accomplishment: Celebrating your success after completing a habit provides a sense of accomplishment. This sense of accomplishment reinforces the behavior and makes it more likely that you will continue with the habit.

- Celebrating success creates a positive feedback loop: Celebrating your success after completing a habit creates a positive feedback loop. The positive feedback loop reinforces the behavior and makes it more likely that you will continue with the habit.

## 1% Better Every Day Video

### Question 5 - Your takeaways from the video (Minimum 5 points)

- Improvement is not about radical changes but rather small, consistent improvements every day.
- Focusing on small improvements over time can lead to significant progress.
- Habits are the foundation of small improvements.
- Habits are formed through consistent, repetitive actions.
- The key to building habits is to start small and gradually increase the difficulty.
- The environment in which we live and work greatly influences our habits.
- It's important to design our environment to make positive habits easier and negative habits harder.
- Tracking progress can be a powerful motivator for continued improvement.
- Celebrating small wins along the way can help us stay motivated and committed to our goals.

## Book Summary of Atomic Habits

### Question 6 - Write about the book's perspective on habit formation from the lens of Identity, processes and outcomes.

- Sure, here's an overview of the book's perspective on habit formation from the lens of identity, processes, and outcomes:

- Identity: The book emphasizes the importance of identity in habit formation. It argues that lasting change comes from changing our self-image, rather than relying solely on willpower or external motivation.
The author suggests that we should focus on creating new identities for ourselves that are aligned with our desired habits.

- Processes: The book suggests that habits are formed through a three-step process: cue, routine, and reward. The cue is the trigger that signals the start of the habit, the routine is the behavior itself, and the reward is the outcome that reinforces the behavior. The author suggests that we should focus on identifying the cues and rewards that drive our current habits, and then work to replace the routine with a new, desired behavior. The book emphasizes the importance of repetition in habit formation. The more we repeat a behavior, the more automatic it becomes, and the less effort it takes to maintain.

- Outcomes: The book suggests that the ultimate outcome of habit formation is a change in our behavior, which leads to a change in our results. The author suggests that we should focus on small, incremental changes in behavior, rather than trying to make dramatic changes all at once. This allows us to build momentum and make lasting changes. The book emphasizes the importance of tracking our progress and celebrating our successes along the way. This helps to reinforce our new identity and build momentum for further change.

### Question 7 - Write about the book's perspective on how to make a good habit easier.

- Starting small: The author suggests starting with a tiny habit that is easy to do consistently. This helps to build momentum and confidence.

- Removing barriers: One way to make a good habit easier is to remove any obstacles that might prevent you from doing it. For example, if you want to exercise in the morning, you could lay out your workout clothes the night before.

- Making it enjoyable: If you can find a way to make your good habit enjoyable, you are more likely to stick with it. For example, if you want to read more, you could join a book club or find a reading buddy to make it more fun.

- Creating a supportive environment: Surrounding yourself with people who support your good habit can make them easier to maintain. For example, if you want to eat healthier, you could join a cooking class or start a healthy recipe swap with friends.

- Using visual cues: The book suggests using visual cues to remind yourself of your good habit. For example, if you want to drink more water, you could place a water bottle on your desk as a reminder.

### Question 8 - Write about the book's perspective on making a bad habit more difficult.

- Increase the friction: One way to make a bad habit more difficult is to increase the friction involved in performing it. For example, if you want to break the habit of watching too much TV, you can unplug the TV and move it to another room or make it difficult to access the remote control.

- Use commitment devices: A commitment device is a way to commit yourself to not engaging in a bad habit. For example, if you have a habit of overspending, you can give your credit cards to a trusted friend or family member who can help you stick to a budget.

- Change your environment: If your environment is conducive to engaging in bad habits, it can be helpful to make changes that discourage those habits. For example, if you have a habit of snacking on unhealthy food, you can remove the junk food from your house and replace it with healthy snacks.

- Make the consequences more immediate: One reason why bad habits are hard to break is that the negative consequences often happen in the future, while the immediate gratification of the habit is more appealing. To make a bad habit more difficult, you can try to make the negative consequences more immediate. For example, if you have a habit of staying up late and not getting enough sleep, you can set an early alarm that forces you to get up early in the morning, making you feel the consequences of not getting enough sleep right away.

- Use implementation intentions: Implementation intentions involve planning in advance how you will handle situations that may trigger a bad habit. For example, if you have a habit of smoking when you're stressed, you can make a plan to do a breathing exercise or go for a walk instead of smoking when you feel stressed.

- Find a support system: Breaking a bad habit can be challenging, but having a support system can make it easier. You can find a friend, family member, or support group to help you stay accountable and provide encouragement.

## Reflection

### Question 9 - Pick one habit that you would like to do more of. What are the steps that you can take to make it make the cue obvious or the habit more attractive or easy and or response satisfying?

- Start small: Pick a small habit that you want to do more of, like spending 10 minutes a day learning a new programming language or tool. This makes it easier to integrate into your routine and builds momentum.

- Make it easy: Set up your environment to make it as easy as possible to do the habit. For example, if you want to learn a new programming language, have the resources and tools you need readily available and easily accessible.

- Create a trigger: Choose a specific cue that will remind you to do the habit. For example, you can set a reminder on your phone or link the habit to a daily routine like having your morning coffee.

- Make it rewarding: Create a positive association with the habit by rewarding yourself after completing it. This could be as simple as taking a break to do something you enjoy or marking off the habit on a habit tracker.

- Celebrate small successes: Recognize and celebrate even the smallest successes. This reinforces the habit and motivates you to continue.

### Question 10 - Pick one habit that you would like to eliminate or do less of. What are the steps that you can take to make it make the cue invisible or the process unattractive or hard or the response unsatisfying?
- Identify the habit: First, I need to identify the habit that I want to eliminate or do less of. It could be anything, such as scrolling through social media during work hours.

- Make the cue invisible: I can make the cue invisible by turning off notifications on my phone or computer, or by keeping my phone out of sight during work hours.

- Make the process unattractive: I can make the process unattractive by reminding myself of the negative consequences of the habit. For example, I can remind myself that scrolling through social media during work hours will lead to lower productivity and may result in missed deadlines.

- Make the response unsatisfying: I can make the response unsatisfying by replacing the habit with a more positive behavior that is rewarding in the long run. For example, I can replace scrolling through social media with taking a short walk or doing a quick exercise routine to recharge my energy and focus.

- Use positive reinforcement: I can use positive reinforcement to reward myself for not engaging in the habit. For example, I can treat myself to a favorite snack or take a break to do something I enjoy after successfully avoiding the habit for a set amount of time.

## References

- [Youtube | Tiny Habits - BJ Fogg](https://www.youtube.com/watch?v=AdKUJxjn-R8)
- [Youtube | Tiny Habits by BJ Fogg - Core Message](https://www.youtube.com/watch?v=S_8e-6ZHKLs)
- [Youtube | 1% Better Every Day Video](https://www.youtube.com/watch?v=mNeXuCYiE0U)
- [Youtube | Book Summary of Atomic Habits](https://www.youtube.com/watch?v=YT7tQzmGRLA)