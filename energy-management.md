# Energy Management

## Manage Energy not Time

### Question 1: What are the activities you do that make you relax - Calm quadrant?

- I watch youtube on off-topic things to divert my mind to relax.
- I watch good movies to get calm.
- I play video games to get relax.
- I watch cricket highlights to get calm.
- I spend time with my friends.

### Question 2: When do you find getting into the Stress quadrant?

- When I know I have to challenge myself to become better at work by tomorrow.
- When India is losing against Pakistan in cricket.
- When I regret that I could have done something for my family/friends/colleagues so he/she not have been in a bad situation.


### Question 3: How do you understand if you are in the Excitement quadrant?

- When I get surprised by a fact that changes my mind about a particular thing/process works.
- When India is winning any cricket match.
- When I play and win a very good match in sports or video-game.
- When my salary is credited to my account.
- Wheb I see my portfolio is going up.

## Sleep is your superpower

### Question 4 - Paraphrase the Sleep is your Superpower video in detail.

- Lack of sleep can cause negative impacts on both male and female reproductive health.
- Sleep is essential for brain functions such as learning and memory.
- Sleep is necessary both before and after learning.
- Without sleep, memory circuits become waterlogged and unable to absorb new memories.
- Research showed that sleep deprivation caused a 40% deficit in the ability of the brain to make new memories.
- Sleep deprivation shuts down the memory inbox of the brain, making it difficult to commit new experiences to memory.
- Good quality sleep is necessary to restore and enhance memory and learning ability each day.
- Sleep spindles are bursts of electrical activity during deep sleep that help in transferring memories from short-term to long-term storage.
- Disruption of deep sleep contributes to cognitive and memory decline in aging and Alzheimer's disease.
- The potential silver lining is that understanding the physiology of sleep can help improve memory and learning ability, as well as prevent cognitive decline.


### Question 5 - What are some ideas that you can implement to sleep better?

- Establish a regular sleep schedule: Going to bed and waking up at the same time every day helps regulate the body's internal clock and improve the overall quality of sleep.

- Create a sleep-conducive environment: Ensure that the bedroom is cool, quiet, and dark. Use comfortable bedding and pillows.

- Limit exposure to screens: The blue light emitted by screens can disrupt the body's natural sleep-wake cycle. Avoid using phones, tablets, and computers for at least an hour before bedtime.

- Engage in relaxation techniques: Deep breathing exercises, meditation, and progressive muscle relaxation can help calm the mind and promote relaxation.

## Brain Changing Benefits of Exercise

### Question 6 - Paraphrase the video - Brain Changing Benefits of Exercise. Minimum 5 points.

- Physical activity has immediate and long-lasting benefits for the brain, including improved mood, focus, and protection from conditions such as depression, Alzheimer's, and dementia.
- The prefrontal cortex and the hippocampus are two important areas of the brain that are critical for decision-making, attention, personality, and the formation and retention of new long-term memories.
- The speaker, a neuroscientist, was fascinated with the hippocampus and wanted to study the activity of individual brain cells in the hippocampus to understand how brief bursts of electrical activity enable us to form new memories.
- The speaker realized that he had gained weight, had no social life, and spent too much time listening to brain cells in a dark room, which made him miserable. He decided to change his lifestyle and started going to the gym and trying different exercise classes.
- The speaker noticed a mood boost and energy boost after every sweat-inducing workout and eventually lost 25 pounds. A year and a half into his regular exercise program, the speaker noticed that he was able to focus and maintain his attention for longer and that his long-term memory seemed better.
- The speaker discovered exciting and growing literature that showed that physical activity has the same effects on mood, energy, memory, and attention that he had experienced in himself.

### Question 7 - What are some steps you can take to exercise more?

- Set a goal: Determine what type of exercise you want to do and how often you want to do it. It could be as simple as walking for 30 minutes a day or as ambitious as training for a marathon.
- Create a schedule: Set aside specific times during the week for exercise and stick to them. Treat them like any other appointment.
- Find an activity you enjoy: Exercise doesn't have to be boring or painful. Find an activity that you enjoy, such as dancing, swimming, or hiking.
- Mix it up: Vary your workouts to keep things interesting and challenge different parts of your body. Incorporate strength training, cardio, and stretching into your routine.
- Get a workout buddy: Find a friend or family member who shares your exercise goals and make it a social activity.
- Track your progress: Keep track of your workouts and celebrate your achievements, no matter how small. This will help you stay motivated and see how far you've come.

## References

- Emotional Quadrant - https://www.researchgate.net/publication/335191634/figure/fig2/AS:792212367486976@1565889555183/Modified-PA-plane-with-four-emotional-quadrants.jpg
- Sleep is your superpower video - 19:18 minutes - https://www.youtube.com/watch?v=5MuIMqhT8DM
- Brain Changing Benefits of Exercise video - 13:02 minutes - https://www.youtube.com/watch?v=BHY0FxzoKZE