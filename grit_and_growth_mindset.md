# Grit and Growth Mindset

## Grit

### Paraphrase (summarize) the video in a few lines

- She observed that IQ was not the only factor that determined a student's performance; some high-IQ students struggled while some lower-IQ students excelled.
- She discovered that grit, defined as passion and perseverance for long-term goals, was a significant predictor of success, even more so than IQ, good looks or physical health.
- One promising approach is the concept of a "growth mindset," which suggests that the ability to learn is not fixed and can improve with effort.
- She emphasized the need to test and measure the effectiveness of different strategies for building grit in children, and the willingness to learn from failures and start over with new lessons.
- The ultimate goal is to help children develop grit and become more resilient in the face of challenges.

### What are your key takeaways from the video to take action on?

- Recognize that IQ is not the only determinant of success: While IQ is often used as a measure of intelligence and potential for success, it's not the only factor. Grit, which is defined as passion and perseverance for long-term goals, is identified as a significant predictor of success.

- Acknowledge that talent alone does not make one gritty: Talent and natural abilities are not enough to ensure success. Grit is unrelated or even inversely related to measures of talent, indicating that hard work, perseverance, and resilience are equally, if not more, important.

## Introduction to Growth Mindset

### Paraphrase (summarize) the video in a few lines in your own words

- A growth mindset is a concept that is changing and improving the way people learn in the fields of sports and education.
- There are two types of mindsets: Fixed mindset and growth mindset. People with a fixed mindset believe that skills and intelligence are set, while people with a growth mindset believe that skills and intelligence can be developed.
- Mindsets have a major influence on people's ability to learn, and those with a growth mindset tend to learn, grow, and achieve more over time.
- Companies, sports teams, and schools around the world are implementing a growth mindset into their culture.
- A fixed mindset is characterized by the belief that skills are born, focus on performance and outcomes, avoidance of effort and challenges, discouragement by mistakes, and defensiveness towards feedback.
- People with a fixed mindset shy away from effort because they believe they cannot change. They give up when faced with challenges and view them as threats to their self-image and dislike making mistakes and get discouraged by them as it makes them look bad and is defensive and does not see the value of feedback from others

### What are your key takeaways from the video to take action on?

- Embrace a growth mindset: Understanding that skills and intelligence can be developed through effort and learning, rather than being fixed traits, can lead to a more positive and proactive approach toward personal and professional growth.

- Recognize the influence of mindsets: Being aware of the impact of mindsets on my ability to learn and achieve is important.
  
- Emphasize effort and challenges: Rather than avoiding effort and challenges due to fear of failure or mistakes, I should embrace them as opportunities for growth.

- Embrace feedback: Feedback, even if it points out areas for improvement, should be seen as valuable input for growth.

- Encourage a growth mindset culture: Whether it's in my workplace, sports team, or educational setting, I should strive to promote a culture that embraces a growth mindset.

- Recognize the limitations of a fixed mindset: I should be aware of the negative aspects of a fixed mindset, such as avoiding effort, giving up easily, and being defensive towards feedback.

## Understanding Internal Locus of Control

### What is the Internal Locus of Control? What is the key point in the video?

- The study conducted at Columbia University by Professor Claudia M. Mueller involved fifth graders working on puzzles and being given feedback on their performance.

- Half of the students were told they did well because they were smart, while the other half were told they did well because they worked hard. 

- The students who were told they did well because they were smart exhibited lower levels of motivation, spent more time on easy puzzles, and overall showed less effort in solving the puzzles.

- The students who were told they did well because they worked hard exhibited higher levels of motivation, spent more time on challenging puzzles, and overall showed more effort in solving the puzzles.

- The concept of locus of control, which is the degree to which one believes they have control over their life, was discussed in the study.

- The students who were told they did well because they were smart were led to believe in an external locus of control, while the students who were told they did well because they worked hard were led to believe in an internal locus of control.

- Having an internal locus of control, where one feels they have control over their life and are responsible for their outcomes, is associated with higher levels of motivation.

- The curse of having an external locus of control is that it can lead to a lack of motivation and effort, as one may feel that external factors are beyond their control.

- Adopting an internal locus of control can be achieved by solving problems in one's own life and appreciating the efforts and actions taken to solve them.

- Building a belief in one's control over their destiny and having an internal locus of control can lead to improved motivation and a lack of issues with motivation in life.


## How to build a Growth Mindset

### Paraphrase (summarize) the video in a few lines in your own words.

- Believe in your ability to figure things out and get better. This fundamental belief in yourself enables lifelong improvement and growth.

- Question your assumptions and limiting beliefs. Don't let your current knowledge, skills, or abilities box in or narrow down your vision for the future.

- Develop your own life curriculum. Take charge of your own learning and development by actively seeking out resources such as books, seminars, and other learning opportunities that align with your passions and dreams.

- Embrace a growth mindset by being open to learning, taking on challenges, and persisting despite setbacks or failures.

- Cultivate a positive attitude towards failure and mistakes, viewing them as opportunities for learning and growth rather than reasons for giving up.

- Surround yourself with positive and supportive people who believe in your potential and encourage your growth.

- Practice self-compassion and forgiveness, acknowledge that growth and improvement take time and effort, and be kind to yourself during the process.

- Set realistic and achievable goals, and break them down into smaller steps to make progress manageable and sustainable.

- Stay curious and open-minded, embracing new experiences, perspectives, and feedback as opportunities for growth and learning.

- Stay committed and persistent in your pursuit of growth, even when faced with challenges, setbacks, or self-doubt. Keep pushing yourself outside of your comfort zone and never stop believing in your ability to improve and grow.

### What are your key takeaways from the video to take action on?

- Prioritize health and well-being: It's important to prioritize my physical and mental health and make time for self-care activities such as exercise, meditation, and hobbies.

- Set clear goals: Having clear goals helps me stay focused and motivated.

- Manage time effectively: Time management is crucial for maximizing productivity and achieving my goals.

- Cultivate positive relationships: Building and maintaining positive relationships with family, friends, and colleagues is important for my overall well-being and success.

- Continuously learn and grow: Lifelong learning is essential for personal and professional growth.

- Take calculated risks: Taking risks is a part of personal and professional growth.

- Practice self-reflection: Regular self-reflection allows me to evaluate my progress, identify my strengths and weaknesses, and make necessary adjustments.
  
- Give back to others: Helping others and giving back to the community is not only fulfilling but also promotes personal growth and happiness.
  
- Maintain a healthy work-life balance: Achieving a balance between work and personal life is important for overall well-being.

## Mindset - A MountBlue Warrior Reference Manual

### What are one or more points that you want to take action on from the manual? (Maximum 3)

- Stay committed to completing problems: Emphasize the importance of not quitting a problem until it is completed.

- Prioritize understanding before coding: Make sure you have a solid understanding of a concept before attempting to write code. This may involve thorough research, utilizing resources like documentation, Google, Stack Overflow, Github Issues, and the internet to gain a deep understanding of the concept.

- Take ownership of projects: Make a conscious effort to take ownership of the projects assigned to you. This includes not only the execution and delivery of the projects but also taking responsibility for their functionality and quality.

- Embrace confusion, discomfort, and errors: Shift your mindset towards embracing confusion, discomfort, and errors as part of the learning process. Recognize that these challenges are opportunities for growth and improvement.

- Follow a systematic problem-solving approach: Adopt a systematic approach to problem-solving, including steps like relaxation, focusing on understanding the problem, researching and gathering relevant information, coding, and repeating the process as needed.

## References
- Grit Video - https://www.youtube.com/watch?v=H14bBuluwB8
- Introduction to Growth Mindset Video - https://www.youtube.com/watch?v=75GFzikmRY0
- How to stay motivated Video - The Locus Rule - https://www.youtube.com/watch?v=8ZhoeSaPF-k
- How to build a Growth Mindset Video - https://www.youtube.com/watch?v=9DVdclX6NzY
- Mindset Docs - https://docs.google.com/document/d/1SPUqC-8WwfiDlsRGKWqoMtC14v6_2TEhq7LZs29bJWk