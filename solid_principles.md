# SOLID Principles

## Overview
* SOLID principles were introduced by Robert C Martin in his paper **"Design Principles and Design Patterns"** in 2000.
* Later revised by Michael Feather to give SOLID acronym.
* It is the concept of software code design to get more maintainable , understandable and flexible software.
* To reduce complexity and maintainability when the application adds more functions.
* SOLID states for : 
  1. **S**ingle Responsibility
  2. **O**pen/Closed
  3. **L**iskov Substitution
  4. **I**nterface Segregation
  5. **D**ependency Inversion
  
## Single Responsibility Principle
* It states that a class should have one responsibility and so should only have a single reason to change.
* This can help in:
  * Testing : A class with one responsibility will have fewer test cases.
  * Version Control : Easier to track the version control for any update in class.
* Let's see with code samples
    - For example, let's take a class to represent a sample car
    ```
        public class Car{
            public String name;
            pubic double price;

        public Car(String name,double price){
            this.name = name;
            this.price = price
        }
    }
    ```
    - and make a class for an invoice
    ```
    public class Invoice{
        public car Car;
        public double tax;
        public double total;

        public Invoice(Car car,double tax,double total){
            this.car = car;
            this.tax = tax;
            this.total = total;
        }

        public Calculate_total(){
            //Calculate total
        }

        public Print_invoice(){
            //Print invoice
        }
    }
    ```
    - Here there are two responsibilities in one Invoice class to calculate the total and to print an invoice.
    - To follow Single responsibility , another class should be made to print an invoice.

## Open/Closed Principle
* It states that a class should be open for extension but closed for modification.
* A class can add more functions without changing its core responsibility
* This can help in
    - Testing : Do not have to test core responsibility as it is not altered.
    - Bugs : fewer bugs as existing code is mostly untouched.
* Let's see with code samples
    - Let's add a save_invoice function to save invoices in database in our invoice class
    ```
    public class Invoice{
        public car Car;
        public double tax;
        public double total;

        public Invoice(Car car,double tax,double total){
            this.car = car;
            this.tax = tax;
            this.total = total;
        }

        public Calculate_total(){
            //Calculate total
        }

        public save_invoice(){
            //Save invoice in database
        }
    }
    ```
    - Here added more functions to the invoice class without altering the existing class.

## Liskov Substitution Principle
* It states that subclasses should be substitutable for their base classes.
* If class B is a subclass of class A then class B can replace without knowing if needed.
* Let's see with code samples
    * Let's design our car functions
    ```
        public class Petrol_car implements Car{
            public Engine engine;

            public turn_on(){
                // turn on engine
            }
            public turn_off(){
                // turn off engine
            }
        }
    ```
    - We can use these subclasses for new electric cars
    ```
        public class Electric_car implements Car{
            public Engine engine;

            public turn_on(){
                // turn on engine
            }
            public turn_off(){
                // turn off engine
            }
        }
    ```
    - Here subclass replaces without knowing.

## Interface Segregation Principle
* It states that Clients should not be forced to depend upon interfaces that they do not use.
* You should have many interfaces rather than one fat interface or simply larger interfaces should split into small interfaces.
* Let's see with code samples
    - Let's add acceleration and break functionality to our petrol car
    ```
        public class Petrol_car implements Car{
            public Engine engine;

            public turn_on(){
                // turn on engine
            }
            public turn_off(){
                // turn off engine
            }
            public accelerate(){
                // acceleration value
            }
            public break(){
                // breaks on-off
            }
        }
    ```
    - Here adding these functions to the same class can lead to bugs , so , it is better to implement news classes for them.
    ```
        public class Petrol_car implements Car{
            public Engine engine;

            public turn_on(){
                // turn on engine
            }
            public turn_off(){
                // turn off engine
            }
        }
        public class acceleration implements petrol_car{
            public accelerate(){
                // acceleration value
            }
        }
        public class break implements petrol_car{
            public break(){
                // break on/off
            }
        }
    ```
    - So, we segregated two different situations of acceleration and break.

## Dependency Inversion Principle
* It states that Depends upon abstractions, (not) concretions.
* Change in low-level classes should not affect High-level classes and both should remain in same direction on the dependency of abstraction.
* To reduce the dependency of a high-level class on the low-level class by adding an interface.
* Let's see with code samples
    - In our petrol car , acceleration and break are more connected but have two classes , so we can introduce a new interface to make them reliable in each other.
    ```
    public class Petrol_car implements Car{
            public Engine engine;

            public turn_on(){
                // turn on engine
            }
            public turn_off(){
                // turn off engine
            }
        }
        public class speed implemnts petrol_car{
            public speed = speed
            public speed(){
                speed = this.speed
            }
        }
        public class acceleration implements speed{
            public accelerate(){
                // acceleration value
            }
        }
        public class break implements speed{
            public break(){
                // break on/off
            }
        }
    ```
    - Now , adding a speed tracker to track both the acceleration and break of a car will be more flexible and reduces the dependency on petrol_car class to track the speed of the car.

## Conclusion
* This report shows how following some simple design principles of code can reduce complexities and bugs.
* And also leads to more neat , clean , readable and understandable code.
* This shows as we can refactor even an existining code to this type of design for better results.

## References
* https://www.baeldung.com/solid-principles
* https://www.freecodecamp.org/news/solid-principles-explained-in-plain-english/
* https://en.wikipedia.org/wiki/SOLID