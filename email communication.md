# Email Communication

## Email 1 - Hi

### Subject - Hi

Hi,

This is Rahul Bhardwaj. Today is my first day at your company.
Excited to contribute and learn new things in your company.
- I am from Deoghar, Jharkhand
- I have completed B.Tech in Computer Science and Engineering from NSEC Kolkata.
- I have experience in Data Science as Data Analyst at Tiger Analytics mainly working on Python, Pandas, Numpy and Machine Learning.
- I love to learn new off-topic things like global geopolitics.

I'm eager to learn more about Company's culture and values, and I'm excited to be a part of such a talented and dedicated team.

Regards,
Rahul Bhardwaj
+91 8102442670

## Email 2 - Daily Log

### Subject - Daily Logs | Mar 17

Hi,

**Today**
- Completed the JS-FIFA project's first 4 questions with for loops.
- Completed life-skills-track Learning Process assignment.
**Plans for Tomorrow**
- Complete the life-skills-track Email Communication assignment.
- Refactor JS-FIFA project's first 4 questions by using higher-order functions and clean code.

Regards,
Rahul Bhardwaj
+91 8102442670

## Email 3 - Request for Sick Leave

### Subject - Sick Leave | Mar 18-19

Hi,

I am writing to request sick leave for the next two days, March 18 to March 19.
I will be available on mobile for urgent cases.

Regards,
Rahul Bhardwaj
+91 8102442670

## Email 4 - Requesting credentials for Git and Staging Server

### Subject - Request for git and staging server credentials.

Hi,

Could you please provide me with the credentials to access the Git repository and staging server for my project team? We require these tools to manage our code and test our changes before deploying to the live environment. Thank you for your assistance.

Regards,
Rahul Bhardwaj
+91 8102442670

## Email 5 - Request for vacation

### Subject - Request for Diwali Vacation | Mar 27-31

Hi,

I am writing to request a vacation for 5 days from March 27 to March 31 for the upcoming Diwali festival. As this is a special time for my family, I would like to visit my home and spend some time with them.

I will be available via email for any urgent matters.
I appreciate your understanding and support during this time.

Regards,
Rahul Bhardwaj
+91 8102442670

## Email 6 - Requesting Feedback Session

### Subject - Request for Feedback Session

Hi,
​​​​​​​
I would like to schedule a feedback session with you to discuss my performance in the last month. Here is my proposed schedule:
- Date: March 20, Time: 2 pm to 4 pm
- Date: March 21, Time: 4 pm to 6 pm

Please let me know if either of these times is convenient for you. If not, I am open to other suggestions.

Thank you for your time and consideration.

Regards,
Rahul Bhardwaj
+91 8102442670